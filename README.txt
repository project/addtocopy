Module provides jQuery addtocopy functionality which automatically adds
configurable text and link to the text copied from your site, i.e. when
someone copies text from your site and then pastes it - this text will
have additional line like this: "For more details see: [link to the
page this text comes from]".
